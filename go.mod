module gitlab.com/heimdallthor/tssgame

go 1.13

require (
	github.com/cosmos/cosmos-sdk v0.38.0
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.3.3
	github.com/gorilla/mux v1.7.3
	github.com/libp2p/go-libp2p v0.5.1
	github.com/libp2p/go-libp2p-core v0.3.0
	github.com/libp2p/go-libp2p-discovery v0.2.0
	github.com/libp2p/go-libp2p-kad-dht v0.5.0
	github.com/libp2p/go-yamux v1.2.3
	github.com/multiformats/go-multiaddr v0.2.0
	github.com/rs/zerolog v1.17.2
	github.com/tendermint/tendermint v0.33.0
	gitlab.com/thorchain/thornode v0.0.0-20200206212022-bf3172f9f807
)
