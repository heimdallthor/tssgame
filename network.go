package tssgame

import (
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"sync"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/libp2p/go-libp2p"
	"github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/host"
	"github.com/libp2p/go-libp2p-core/network"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/libp2p/go-libp2p-core/protocol"
	discovery "github.com/libp2p/go-libp2p-discovery"
	dht "github.com/libp2p/go-libp2p-kad-dht"
	"github.com/libp2p/go-yamux"
	maddr "github.com/multiformats/go-multiaddr"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.com/heimdallthor/tssgame/messages"
)

const (
	// JoinGameProtocol use to join a game
	JoinGameProtocol protocol.ID = "/thorchain/joingame"
)
const (
	// LengthHeader represent how many bytes we used as header
	LengthHeader = 4
	// MaxPayload the maximum payload for a message
	MaxPayload = 81920 // 80kb
	// TimeoutReadWrite maximum time to wait on read and write
	TimeoutReadWrite = time.Second * 10
	// TimeoutBroadcast maximum time to wait for message broadcast
	TimeoutBroadcast = time.Minute * 5
	// TimeoutConnecting maximum time for wait for peers to connect
	TimeoutConnecting = time.Minute * 1
)

type OnJoinGameHandler func(jgt *JoinGameRequest) error

type Network struct {
	rendezvous       string // based on group
	logger           zerolog.Logger
	listenAddr       maddr.Multiaddr
	host             host.Host
	routingDiscovery *discovery.RoutingDiscovery
	wg               *sync.WaitGroup
	bootstrapPeers   []maddr.Multiaddr
	stopChan         chan struct{} // channel to indicate whether we should stop
	JoinGameHandler  OnJoinGameHandler
}

// NewNetwork create a new instance of network
func NewNetwork(rendezvous string, bootstrapPeers []maddr.Multiaddr, ipAddr string, port int, handler OnJoinGameHandler) (*Network, error) {
	if len(ipAddr) == 0 {
		ipAddr = "0.0.0.0" // bind to all interface
	}
	addr, err := maddr.NewMultiaddr(fmt.Sprintf("/ip4/%s/tcp/%d", ipAddr, port))
	if err != nil {
		return nil, fmt.Errorf("fail to create listen addr: %w", err)
	}
	return &Network{
		rendezvous:      rendezvous,
		bootstrapPeers:  bootstrapPeers,
		logger:          log.With().Str("module", "network").Logger(),
		listenAddr:      addr,
		wg:              &sync.WaitGroup{},
		stopChan:        make(chan struct{}),
		JoinGameHandler: handler,
	}, nil
}

func (n *Network) Start(privKeyBytes []byte) error {
	ctx := context.Background()
	p2pPriKey, err := crypto.UnmarshalSecp256k1PrivateKey(privKeyBytes)
	if err != nil {
		n.logger.Error().Msgf("error is %f", err)
		return err
	}

	h, err := libp2p.New(ctx,
		libp2p.ListenAddrs([]maddr.Multiaddr{n.listenAddr}...),
		libp2p.Identity(p2pPriKey),
	)
	if err != nil {
		return fmt.Errorf("fail to create p2p host: %w", err)
	}
	n.host = h
	n.logger.Info().Msgf("Host created, we are: %s, at: %s", h.ID(), h.Addrs())

	h.SetStreamHandler(JoinGameProtocol, n.joinGame)
	// Start a DHT, for use in peer discovery. We can't just make a new DHT
	// client because we want each peer to maintain its own local copy of the
	// DHT, so that the bootstrapping node of the DHT can go down without
	// inhibiting future peer discovery.
	kademliaDHT, err := dht.New(ctx, h)
	if err != nil {
		return fmt.Errorf("fail to create DHT: %w", err)
	}
	n.logger.Debug().Msg("Bootstrapping the DHT")
	if err = kademliaDHT.Bootstrap(ctx); err != nil {
		return fmt.Errorf("fail to bootstrap DHT: %w", err)
	}
	if err := n.connectToBootstrapPeers(); nil != err {
		return fmt.Errorf("fail to connect to bootstrap peer: %w", err)
	}
	// We use a rendezvous point "meet me here" to announce our location.
	// This is like telling your friends to meet you at the Eiffel Tower.

	routingDiscovery := discovery.NewRoutingDiscovery(kademliaDHT)
	discovery.Advertise(ctx, routingDiscovery, n.rendezvous)
	n.routingDiscovery = routingDiscovery
	n.logger.Info().Msg("Successfully announced!")
	// n.wg.Add(1)
	// go n.discoverNewPeers()
	return nil
}

func (n *Network) joinGame(stream network.Stream) {
	defer func() {
		if err := stream.Close(); err != nil {
			n.logger.Err(err).Msg("fail to close the stream")
		}
	}()
	peerID := stream.Conn().RemotePeer()
	n.logger.Debug().Msgf("reading from stream of peer: %s", peerID)
	length := make([]byte, LengthHeader)
	// set read header timeout
	if err := stream.SetReadDeadline(time.Now().Add(time.Second * 5)); nil != err {
		n.logger.Error().Err(err).Msgf("fail to set read header timeout,peerID:%s", peerID)
		stream.Reset()
		return
	}
	h, err := stream.Read(length)
	if err != nil {
		if errors.Is(err, io.EOF) {
			return
		}
		n.logger.Error().Err(err).Msgf("fail to read from header from stream,peerID: %s", peerID)

		return
	}
	if h < LengthHeader {
		n.logger.Error().Msgf("short read, we only read :%d bytes", n)
		return
	}
	l := binary.LittleEndian.Uint32(length)
	// we are transferring protobuf messages , how big can that be , if it is larger then MaxPayload , then definitely no no...
	if l > MaxPayload {
		n.logger.Warn().Msgf("peer:%s trying to send %d bytes payload", peerID, l)
		return
	}
	buf := make([]byte, l)
	if err := stream.SetReadDeadline(time.Now().Add(TimeoutReadWrite)); nil != err {
		n.logger.Error().Err(err).Msg("fail to set read deadline")
		stream.Reset()
		return
	}
	h, err = stream.Read(buf)
	if err != nil {
		n.logger.Error().Err(err).Msgf("fail to read from stream,peerID: %s", peerID)
		return
	}
	if uint32(h) != l {
		// short reading
		n.logger.Error().Err(err).Msgf("we are expecting %d bytes , but we only got %d", l, n)
		return
	}
	var msg messages.JoinGame
	if err := proto.Unmarshal(buf, &msg); err != nil {
		n.logger.Err(err).Msg("fail to unmarshal join game message")
		return
	}
	resp, err := n.processJoinGame(peerID, &msg)
	if err != nil {
		n.logger.Err(err).Msg("fail to process join game message")
		return
	}
	n.writeJoinGameResp(stream, resp)
}
func (n *Network) processJoinGame(peerID peer.ID, msg *messages.JoinGame) (*JoinGameResponse, error) {
	jgt := &JoinGameRequest{
		ID:        msg.ID,
		Threshold: msg.Threshold,
		PeerID:    peerID,
		resp:      make(chan JoinGameResponse, 1),
	}

	if n.JoinGameHandler != nil {
		if err := n.JoinGameHandler(jgt); err != nil {
			if err != ErrGameAlreadyStarted {
				n.logger.Err(err).Msgf("fail to join the game:%s", jgt.ID)
				return nil, err
			}
			resp := &JoinGameResponse{
				ID:      msg.ID,
				Status:  AlreadyStarted,
				Players: nil,
			}
			return resp, nil
		}
	}

	select {
	case r := <-jgt.resp:
		return &r, nil
	case <-time.After(time.Minute):
		n.logger.Error().Msg("fail to join game after 1 minute timeout")
		return &JoinGameResponse{
			ID:      msg.ID,
			Status:  Timeout,
			Players: nil,
		}, nil
	}
}
func (n *Network) writeJoinGameResp(stream network.Stream, resp *JoinGameResponse) {
	buf, err := resp.GetJoinGameResponseBytes()
	if err != nil {
		n.logger.Err(err).Msg("fail to get join game response")
		return
	}
	n.logger.Info().Msgf("writing response(%+v) back to:%s", resp, stream.Conn().RemotePeer())
	_, err = stream.Write(buf)
	if nil != err {
		n.logger.Err(err).Msg("fail to write response to stream")
	}
}

func (n *Network) discoverNewPeers() {
	defer n.wg.Done()
	for {
		select {
		case <-n.stopChan:
			return
		case <-time.After(time.Second):
			n.discover()
		}
	}

}

func (n *Network) discover() {
	ctx, cancel := context.WithTimeout(context.Background(), TimeoutBroadcast)
	defer cancel()
	peerChan, err := n.routingDiscovery.FindPeers(ctx, n.rendezvous)
	if err != nil {
		n.logger.Error().Err(err).Msg("fail to find any peers")
	}

	for {
		select {
		case <-n.stopChan:
			return
		case ai, more := <-peerChan:
			if !more {
				return
			}
			if ai.ID == n.host.ID() {
				continue
			}
			connectCtx, _ := context.WithTimeout(context.Background(), time.Second*30)
			if err := n.host.Connect(connectCtx, ai); err != nil {
				n.logger.Err(err).Msgf("fail to connect to peer:%s", ai)
			}

		}
	}
}

// SendJoinGame send join game message to everyone we know
func (n *Network) JoinGame(pID peer.ID, msg *messages.JoinGame) (*messages.JoinGameResp, error) {
	if pID == n.host.ID() {
		resp, err := n.processJoinGame(pID, msg)
		if err != nil {
			return nil, err
		}
		return resp.GetJoinGameResponse()
	}
	msgBuf, err := proto.Marshal(msg)
	if err != nil {
		return nil, fmt.Errorf("fail to marshal msg to protobuf: %w", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	stream, err := n.host.NewStream(ctx, pID, JoinGameProtocol)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err := stream.Close(); nil != err {
			n.logger.Error().Err(err).Msgf("fail to reset stream to peer(%s)", pID)
		}
	}()
	n.logger.Info().Msg("open stream successfully.")
	length := len(msgBuf)
	buf := make([]byte, LengthHeader)
	binary.LittleEndian.PutUint32(buf, uint32(length))
	if err := stream.SetWriteDeadline(time.Now().Add(TimeoutReadWrite)); nil != err {
		return nil, errors.New("fail to set write deadline")
	}
	l, err := stream.Write(buf)
	if err != nil {
		n.logger.Error().Err(err).Msgf("fail to write to peer : %s", stream.Conn().RemotePeer().String())
		return nil, err
	}
	if l < LengthHeader {
		return nil, fmt.Errorf("short write, we would like to write: %d, however we only write: %d", LengthHeader, n)
	}
	if err := stream.SetWriteDeadline(time.Now().Add(TimeoutReadWrite)); nil != err {
		return nil, errors.New("fail to set write deadline")
	}
	l, err = stream.Write(msgBuf)
	if err != nil {
		return nil, fmt.Errorf("fail to write: %w", err)
	}
	if l < length {
		return nil, fmt.Errorf("short write, we would like to write: %d, however we only write: %d", length, n)
	}
	// read everything
	respBuf, err := ioutil.ReadAll(stream)
	if err != nil {
		if err != yamux.ErrConnectionReset {
			return nil, fmt.Errorf("fail to read response: %w", err)
		}
	}
	if len(respBuf) == 0 {
		return nil, errors.New("fail to get response")
	}
	var resp messages.JoinGameResp
	if err := proto.Unmarshal(respBuf, &resp); err != nil {
		return nil, fmt.Errorf("fail to unmarshal JoinGameResp: %w", err)
	}
	return &resp, nil
}

func (n *Network) connectToBootstrapPeers() error {
	// Let's connect to the bootstrap nodes first. They will tell us about the
	// other nodes in the network.
	var wg sync.WaitGroup
	for _, peerAddr := range n.bootstrapPeers {
		pi, err := peer.AddrInfoFromP2pAddr(peerAddr)
		if err != nil {
			return fmt.Errorf("fail to add peer: %w", err)
		}
		wg.Add(1)
		go func() {
			defer wg.Done()
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
			defer cancel()
			if err := n.host.Connect(ctx, *pi); err != nil {
				n.logger.Error().Err(err).Msgf("fail to connect to peer:%+v", pi.Loggable())
				return
			}
			n.logger.Info().Msgf("Connection established with bootstrap node: %s", *pi)
		}()
	}
	wg.Wait()
	return nil
}
