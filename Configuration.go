package tssgame

import (
	"strings"

	maddr "github.com/multiformats/go-multiaddr"
)

type Configuration struct {
	HttpPort       int      `json:"http_port"`
	P2PAddr        string   `json:"p2p_addr"`
	P2PPort        int      `json:"p2p_port"`
	BootstrapPeers addrList `json:"bootstrap_peers"`
}
type addrList []maddr.Multiaddr

func (al *addrList) String() string {
	strs := make([]string, len(*al))
	for i, addr := range *al {
		strs[i] = addr.String()
	}
	return strings.Join(strs, ",")
}

func (al *addrList) Set(value string) error {
	addr, err := maddr.NewMultiaddr(value)
	if err != nil {
		return err
	}
	*al = append(*al, addr)
	return nil
}
