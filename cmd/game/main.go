package main

import (
	"bufio"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"

	"github.com/cosmos/cosmos-sdk/client/input"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	cryptokey "github.com/tendermint/tendermint/crypto"
	"github.com/tendermint/tendermint/crypto/secp256k1"
	"gitlab.com/thorchain/thornode/cmd"

	"gitlab.com/heimdallthor/tssgame"
)

func setupBech32Prefix() {
	config := sdk.GetConfig()
	config.SetBech32PrefixForAccount(cmd.Bech32PrefixAccAddr, cmd.Bech32PrefixAccPub)
	config.SetBech32PrefixForValidator(cmd.Bech32PrefixValAddr, cmd.Bech32PrefixValPub)
	config.SetBech32PrefixForConsensusNode(cmd.Bech32PrefixConsAddr, cmd.Bech32PrefixConsPub)
}
func initFlags() tssgame.Configuration {
	var cfg tssgame.Configuration
	flag.IntVar(&cfg.HttpPort, "port", 8080, "http port allow user to submit request")
	flag.StringVar(&cfg.P2PAddr, "addr", "", "P2P address")
	flag.IntVar(&cfg.P2PPort, "pport", 5678, "p2p port")
	flag.Var(&cfg.BootstrapPeers, "peer", "peers used to bootstrap p2p network")
	flag.Parse()
	return cfg
}

func initLog(level string, pretty bool, serviceValue string) {
	l, err := zerolog.ParseLevel(level)
	if err != nil {
		log.Warn().Msgf("%s is not a valid log-level, falling back to 'info'", level)
	}
	var out io.Writer = os.Stdout
	if pretty {
		out = zerolog.ConsoleWriter{Out: os.Stdout}
	}
	zerolog.SetGlobalLevel(l)
	log.Logger = log.Output(out).With().Str("service", serviceValue).Logger()
}

func getPriKey(priKeyString string) (cryptokey.PrivKey, error) {
	priHexBytes, err := base64.StdEncoding.DecodeString(priKeyString)
	if err != nil {
		return nil, fmt.Errorf("fail to decode private key: %w", err)
	}
	rawBytes, err := hex.DecodeString(string(priHexBytes))
	if err != nil {
		return nil, fmt.Errorf("fail to hex decode private key: %w", err)
	}
	var keyBytesArray [32]byte
	copy(keyBytesArray[:], rawBytes[:32])
	priKey := secp256k1.PrivKeySecp256k1(keyBytesArray)
	return priKey, nil
}

func getPriKeyRawBytes(priKey cryptokey.PrivKey) ([]byte, error) {
	var keyBytesArray [32]byte
	pk, ok := priKey.(secp256k1.PrivKeySecp256k1)
	if !ok {
		return nil, errors.New("private key is not secp256p1.PrivKeySecp256k1")
	}
	copy(keyBytesArray[:], pk[:])
	return keyBytesArray[:], nil
}

func main() {
	setupBech32Prefix()
	cfg := initFlags()
	initLog("debug", true, "tss-game")
	inBuf := bufio.NewReader(os.Stdin)
	priKeyBytes, err := input.GetPassword("input node secret key:", inBuf)
	if err != nil {
		fmt.Printf("error in get the secret key: %s\n", err.Error())
		return
	}
	priKey, err := getPriKey(priKeyBytes)
	if err != nil {
		log.Fatal().Err(err).Msg("fail to get private key")
	}
	rawBytes, err := getPriKeyRawBytes(priKey)
	if err != nil {
		log.Fatal().Err(err).Msg("fail to get private key raw bytes")
	}
	s, err := tssgame.NewServer(cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("fail to create server object")
	}
	if err := s.Start([]byte(rawBytes)); err != nil {
		log.Err(err).Msg("fail to start")
	}
}
