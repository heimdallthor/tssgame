echo "password"|thorcli keys add first
echo "password"|thorcli keys add second
echo "password"|thorcli keys add third
echo "password"|thorcli keys add fourth
echo "password"|thorcli keys tss first|./game -port 8080 -pport 5678
export FIRST=ODQ5YmU0ZDhjMjkxYmU5OWZlOWIwMzE3OWNlYjE3MDk1MTdkMTZkNjlkZTkzYzdkMjFlMzJkOTZiYjU5ZGJkZA==
export SECOND=ZjI5MjA5ZmZhOGZkNjgxNjk4ZmQxMWNkMmZkM2JjMTQxYjJkNjRiNTJjMmM3Y2E1Zjg3ZTU1YWU0MWEyY2U3Mg==
/ip4/127.0.0.1/tcp/5678/ipfs/16Uiu2HAm1PcCAcUZd6N4RZWnbmBHjb14Hm5iE98BY6xi7R4otHCP
echo $SECOND|./game -port 8081 -pport 5679 -peer /ip4/127.0.0.1/tcp/5678/ipfs/16Uiu2HAm1PcCAcUZd6N4RZWnbmBHjb14Hm5iE98BY6xi7R4otHCP
echo $THIRD|./game -port 8082 -pport 5680 -peer /ip4/127.0.0.1/tcp/5678/ipfs/16Uiu2HAm1PcCAcUZd6N4RZWnbmBHjb14Hm5iE98BY6xi7R4otHCP
echo $FOURTH|./game -port 8083 -pport 5681 -peer /ip4/127.0.0.1/tcp/5678/ipfs/16Uiu2HAm1PcCAcUZd6N4RZWnbmBHjb14Hm5iE98BY6xi7R4otHCP

curl --location --request POST 'http://localhost:8081/game' \
--header 'Content-Type: application/json' \
--data-raw '{
    "message":"61736C646B6A666173646B6A66686B6A7177657271776B6A6572686A6B6A6B617364666A6B686173666A6B686173646B6A6668736461",
    "players":[
        "thorpub1addwnpepqfvfatd8wqkrl4mrzhp3ruu9262gv97w4flshtevhaf9zfshwzzgc7mj2xn",
        "thorpub1addwnpepqga5cupfejfhtw507sh36fvwaekyjt5kwaw0cmgnpku0at2a87qqkp60t43",
        "thorpub1addwnpepqtctt9l4fddeh0krvdpxmqsxa5z9xsa0ac6frqfhm9fq6c6u5lck5s8fm4n",
        "thorpub1addwnpepqv3jdyyk7v3sycfwptf7efkw32nftjs53uwgkmd4kny3yh4gerdgxjd267u"
    ]
}'

curl --location --request POST 'http://localhost:8080/game' \
--header 'Content-Type: application/json' \
--data-raw '{
    "message":"61736C646B6A666173646B6A66686B6A7177657271776B6A6572686A6B6A6B617364666A6B686173666A6B686173646B6A6668736461",
    "players":[
        "thorpub1addwnpepqfvfatd8wqkrl4mrzhp3ruu9262gv97w4flshtevhaf9zfshwzzgc7mj2xn",
        "thorpub1addwnpepqga5cupfejfhtw507sh36fvwaekyjt5kwaw0cmgnpku0at2a87qqkp60t43",
        "thorpub1addwnpepqtctt9l4fddeh0krvdpxmqsxa5z9xsa0ac6frqfhm9fq6c6u5lck5s8fm4n",
        "thorpub1addwnpepqv3jdyyk7v3sycfwptf7efkw32nftjs53uwgkmd4kny3yh4gerdgxjd267u"
    ]
}'

curl --location --request POST 'http://localhost:8082/game' \
--header 'Content-Type: application/json' \
--data-raw '{
    "message":"61736C646B6A666173646B6A66686B6A7177657271776B6A6572686A6B6A6B617364666A6B686173666A6B686173646B6A6668736461",
    "players":[
        "thorpub1addwnpepqfvfatd8wqkrl4mrzhp3ruu9262gv97w4flshtevhaf9zfshwzzgc7mj2xn",
        "thorpub1addwnpepqga5cupfejfhtw507sh36fvwaekyjt5kwaw0cmgnpku0at2a87qqkp60t43",
        "thorpub1addwnpepqtctt9l4fddeh0krvdpxmqsxa5z9xsa0ac6frqfhm9fq6c6u5lck5s8fm4n",
        "thorpub1addwnpepqv3jdyyk7v3sycfwptf7efkw32nftjs53uwgkmd4kny3yh4gerdgxjd267u"
    ]
}'

curl --location --request POST 'http://localhost:8083/game' \
--header 'Content-Type: application/json' \
--data-raw '{
    "message":"61736C646B6A666173646B6A66686B6A7177657271776B6A6572686A6B6A6B617364666A6B686173666A6B686173646B6A6668736461",
    "players":[
        "thorpub1addwnpepqfvfatd8wqkrl4mrzhp3ruu9262gv97w4flshtevhaf9zfshwzzgc7mj2xn",
        "thorpub1addwnpepqga5cupfejfhtw507sh36fvwaekyjt5kwaw0cmgnpku0at2a87qqkp60t43",
        "thorpub1addwnpepqtctt9l4fddeh0krvdpxmqsxa5z9xsa0ac6frqfhm9fq6c6u5lck5s8fm4n",
        "thorpub1addwnpepqv3jdyyk7v3sycfwptf7efkw32nftjs53uwgkmd4kny3yh4gerdgxjd267u"
    ]
}'



