package tssgame

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"hash/fnv"
	"net/http"
	"sort"
	"sync"
	"time"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/gorilla/mux"
	crypto "github.com/libp2p/go-libp2p-core/crypto"
	"github.com/libp2p/go-libp2p-core/peer"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/tendermint/tendermint/crypto/secp256k1"

	"gitlab.com/heimdallthor/tssgame/messages"
)

const (
	Rendezvous string = "tss_game"
)

// Server represent the game server
type Server struct {
	cfg        Configuration
	logger     zerolog.Logger
	wg         *sync.WaitGroup
	httpServer *http.Server
	n          *Network
	stopChan   chan struct{}
	gameLock   *sync.Mutex
	games      map[string]*Game
}

func NewServer(cfg Configuration) (*Server, error) {
	s := &Server{
		cfg:      cfg,
		logger:   log.With().Str("module", "server").Logger(),
		stopChan: make(chan struct{}),
		wg:       &sync.WaitGroup{},
		gameLock: &sync.Mutex{},
		games:    make(map[string]*Game),
	}
	n, err := NewNetwork(Rendezvous, cfg.BootstrapPeers, cfg.P2PAddr, cfg.P2PPort, s.onJoinGame)
	if err != nil {
		return nil, err
	}
	s.n = n
	return s, err
}

func (s *Server) Start(privateKey []byte) error {
	router := mux.NewRouter()
	router.Handle("/game", http.HandlerFunc(s.playGame)).Methods(http.MethodPost)
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", s.cfg.HttpPort),
		Handler: router,
	}
	s.httpServer = server
	if err := s.n.Start(privateKey); err != nil {
		return err
	}
	s.wg.Add(1)
	go s.cleanupFailed()
	return server.ListenAndServe()
}
func (s *Server) playGame(writer http.ResponseWriter, req *http.Request) {
	s.logger.Info().Msg("get playgame request")
	var gameReq GameRequest
	defer func() {
		if err := req.Body.Close(); err != nil {
			s.logger.Err(err).Msg("fail to close request body")
		}
	}()
	dec := json.NewDecoder(req.Body)
	if err := dec.Decode(&gameReq); err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	if err := s.startGame(gameReq); err != nil {
		s.logger.Err(err).Msg("fail to start game")
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
}

var (
	ErrGameAlreadyStarted = errors.New("game already started")
)

func (s *Server) cleanupFailed() {
	defer s.wg.Done()
	for {
		select {
		case <-s.stopChan:
			return
		case <-time.After(time.Second * 30):
			s.gameLock.Lock()
			for k, item := range s.games {
				if time.Since(item.Started) > time.Minute {
					s.logger.Info().Msgf("removing game :%s", k)
					delete(s.games, k)
				}
			}
			s.gameLock.Unlock()
		}
	}
}
func (s *Server) onJoinGame(jgt *JoinGameRequest) error {
	s.logger.Info().Msg("get join game request")
	s.gameLock.Lock()
	defer s.gameLock.Unlock()
	g, ok := s.games[jgt.ID]
	if !ok {
		// first one
		game := &Game{
			ID:        jgt.ID,
			Threshold: jgt.Threshold,
			Status:    GatheringPlayers,
			Started:   time.Now(),
		}
		game.JoinGameRequests = append(game.JoinGameRequests, jgt)
		s.games[jgt.ID] = game
		return nil
	}
	if g.Status == Started {
		return ErrGameAlreadyStarted
	}
	g.JoinGameRequests = append(g.JoinGameRequests, jgt)

	if !g.IsReady() {
		return nil
	}
	response := JoinGameResponse{
		ID:      g.ID,
		Status:  Success,
		Players: g.GetParties(),
	}
	for _, p := range g.JoinGameRequests {
		p.resp <- response
	}
	g.Status = Started

	// will be removed
	return nil
}

func getMasterNode(buf []byte, numNodes int32) (int32, error) {
	h := fnv.New32()
	if _, err := h.Write(buf); err != nil {
		return -1, err
	}
	result := int32(h.Sum32())
	if result < -1 {
		result = result * -1
	}
	fmt.Printf("result:%d", result)
	return result % numNodes, nil

}
func getPeerIDFromSecp256PubKey(pk secp256k1.PubKeySecp256k1) (peer.ID, error) {
	var emptyPeerID peer.ID
	ppk, err := crypto.UnmarshalSecp256k1PublicKey(pk[:])
	if err != nil {
		return emptyPeerID, fmt.Errorf("fail to convert pubkey to the crypto pubkey used in libp2p: %w", err)
	}
	return peer.IDFromPublicKey(ppk)
}

func getPeerIDS(nodes []string) ([]peer.ID, error) {
	var peers []peer.ID
	for _, node := range nodes {
		pk, err := sdk.GetPubKeyFromBech32(sdk.Bech32PubKeyTypeAccPub, node)
		if err != nil {
			return nil, err
		}
		ID, err := getPeerIDFromSecp256PubKey(pk.(secp256k1.PubKeySecp256k1))
		if err != nil {
			return nil, err
		}
		peers = append(peers, ID)
	}
	sort.SliceStable(peers, func(i, j int) bool {
		return peers[i].String() < peers[j].String()
	})
	return peers, nil
}

func (s *Server) startGame(request GameRequest) error {
	buf := []byte(request.Message)
	h := sha256.New()
	if _, err := h.Write(buf); err != nil {
		return err
	}

	master, err := getMasterNode(buf, int32(len(request.Players)))
	if err != nil {
		return err
	}
	peers, err := getPeerIDS(request.Players)
	if err != nil {
		return err
	}
	masterNode := peers[master]
	s.logger.Info().Msgf("master is: %d(%s)", master, masterNode)
	req := &messages.JoinGame{
		ID:        hex.EncodeToString(h.Sum(nil)),
		Threshold: int32(len(request.Players) - 1),
	}
	for _, item := range peers {
		req.Player = append(req.Player, &messages.Player{
			PeerID: item.String(),
		})
	}
	res, err := s.n.JoinGame(masterNode, req)
	if err != nil {
		return err
	}
	s.logger.Info().Msgf("resp:%+v", res)
	return nil
}

func (s *Server) Stop() error {
	return nil
}
