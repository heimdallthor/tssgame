package tssgame

import (
	"sort"
	"time"

	"github.com/gogo/protobuf/proto"
	"github.com/libp2p/go-libp2p-core/peer"

	"gitlab.com/heimdallthor/tssgame/messages"
)

type GameStatus int

const (
	NA GameStatus = iota
	GatheringPlayers
	Started
)

// Game represent the game
type Game struct {
	ID               string             `json:"id"`
	Threshold        int32              `json:"threshold"` // how many player do you need to play the game
	JoinGameRequests []*JoinGameRequest `json:"players"`
	Status           GameStatus         `json:"status"`
	Started          time.Time          `json:"started"`
}

// IsReady do we have enough players to start the game?
func (g *Game) IsReady() bool {
	// we got enough player
	if len(g.JoinGameRequests) >= int(g.Threshold) {
		return true
	}
	return false
}

// GetParties get all the players
func (g *Game) GetParties() []Player {
	if !g.IsReady() {
		return nil
	}
	var players []Player
	for _, item := range g.JoinGameRequests {
		players = append(players, Player{PeerID: item.PeerID})
	}
	sort.SliceStable(players, func(i, j int) bool {
		return players[i].PeerID < players[j].PeerID
	})

	return players
}

// Player potential game player
type Player struct {
	PeerID peer.ID
}

type JoinGameRequest struct {
	ID        string
	PeerID    peer.ID // current peer id who request to join
	Threshold int32
	Players   []Player
	resp      chan JoinGameResponse
}

type JoinGameStatus int

const (
	Unknown JoinGameStatus = iota
	Success
	Timeout
	AlreadyStarted
)

type JoinGameResponse struct {
	ID      string
	Status  JoinGameStatus
	Players []Player
}

func (r *JoinGameResponse) GetJoinGameResponseBytes() ([]byte, error) {
	resp, err := r.GetJoinGameResponse()
	if err != nil {
		return nil, err
	}
	return proto.Marshal(resp)
}

func (r *JoinGameResponse) GetJoinGameResponse() (*messages.JoinGameResp, error) {
	t := messages.JoinGameResp_Unknown
	switch r.Status {
	case Success:
		t = messages.JoinGameResp_Success
	case Timeout:
		t = messages.JoinGameResp_Timeout
	case AlreadyStarted:
		t = messages.JoinGameResp_AlreadyStarted
	}

	resp := &messages.JoinGameResp{
		ID:      r.ID,
		Type:    t,
		Players: nil,
	}
	for _, item := range r.Players {
		resp.Players = append(resp.Players, &messages.Player{
			PeerID: item.PeerID.String(),
		})
	}
	return resp, nil
}
